from tragic_semester_work_2.quicksort import quicksort
import pytest

test_lists = [
    ([10, 7, 8, 9, 1, 5], [1, 5, 7, 8, 9, 10]),
    ([10, 71, 90, -1, 30, -57], [-57, -1, 10, 30, 71, 90]),
    ([-40, 88, -8, 90, 15, 55], [-40, -8, 15, 55, 88, 90]),
    ([10, 20, 40, 50, 1], [1, 10, 20, 40, 50]),
    ([100, 90, 80, 70, 23], [23, 70, 80, 90, 100])
]


@pytest.mark.parametrize("first_list, result_list", test_lists)
def test_quicksort1(first_list, result_list):
    assert quicksort(first_list) == result_list
