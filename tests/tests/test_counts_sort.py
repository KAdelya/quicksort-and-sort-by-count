from tragic_semester_work_2.counts_sort import SimpleCountingSort
import pytest


@pytest.mark.parametrize(
    'test_input, expected',
    [
        ([-110, 7, 8, 11, 88, 5], [-110, 5, 7, 8, 11, 88]),
        ([1, 71, 101, 11, -35, 57], [-35, 1, 11, 57, 71, 101]),
        ([-4, 88, -8, 90, 15, 55], [-8, -4, 15, 55, 88, 90]),
        ([10, 20, 40, 50, 1], [1, 10, 20, 40, 50]),
        ([50, 32, 34, 5, 2, 32], [2, 5, 32, 32, 34, 50])
    ]
)
def test_counts_sort(test_input, expected):
    assert SimpleCountingSort(test_input) == expected
