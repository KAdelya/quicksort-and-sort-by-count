## tragic_semester_work_2

**Install**:

    -pip3 install tragic_semester_work_2


**Usage**:

Help:

    -tragic_semester_work_2 —help

Test generation:

    -tragic_semester_work_2 generate-data 

    -generate_data(start: int, end: int, step: int, count: int):
Test timetables creation:

    -tragic_semester_work_2 measure-time --name_sort quick_sort

    @click.option('--name_sort', type=str, default='quick_sort')
    Choose method by yourself: 'quick_sort' or counting_sort
Graph creation:

    -tragic_semester_work_2 create-chart --file <your table name>
    @click.option('--file', type=str, default='1621659210')

## tragic_semester_work_2

#Install:

    -pip3 install tragic_semester_work_2


#Usage:

Help:

    -tragic_semester_work_2 —help

Test generation:

    -tragic_semester_work_2 generate-data 

    -generate_data(start: int, end: int, step: int, count: int):
Test timetables creation:

    -tragic_semester_work_2 measure-time --name_sort quick_sort

    @click.option('--name_sort', type=str, default='quick_sort')
    Choose method by yourself: 'quick_sort' or counting_sort
Graph creation:

    -tragic_semester_work_2 create-chart --file <your table name>
    @click.option('--file', type=str, default='1621659210')

google drive: https://drive.google.com/drive/folders/13bytMKoxyqp2cRy2gkMMnX-d23qqeQbe

quick_sort graph:
![img.png](img.png)

count_sort graph:
![img_1.png](img_1.png)
